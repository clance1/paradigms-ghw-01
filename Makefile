JC = javac
JVM = java
JVC = class
MAIN = ZooTest

CLASSES = zoo/animals/*

all: 
	$(JC) $(CLASSES).$(JVM)
	$(JC) $(MAIN).$(JVM)

clean:
	rm $(CLASSES).$(JVC)
	rm $(MAIN).$(JVC)
