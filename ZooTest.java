//GHW01
//Michaela Kocher and Carson Lance
//Due 9/11/2019

import zoo.animals.*;
import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;

public class ZooTest{

	//property
	Scanner sc = new Scanner(System.in); // local to ZooTest, accessible to methods

	public static ArrayList<Animal> setupAnimals() throws Exception{
		// this function will read in from the text file and create instances of the 10-15 animals in the zoo
		ArrayList<Animal> animals = new ArrayList<Animal>();

		Scanner s = new Scanner(new File("setup.txt"));

		while(s.hasNext()) {
			String type = s.next();
			String name = s.next();
			addAnimal(type, name, animals);
		}

		return animals;

	} // end of setupAnimals

	public static void printSummaryView(ArrayList<Animal> animals){
		//walk through animals array, count each species as you are walking through it
		int countAnimals = 0;
		int countEagle = 0;
		int countPenguin = 0;
		int countTaipan = 0;
		int countCobra = 0;
		int countLion = 0;
		int countTiger = 0;
		int countBear = 0;

		for(int i=0; i < animals.size(); i++) {
			if (animals.get(i) instanceof Eagle){
				countEagle++;
				countAnimals++;
			}
			if (animals.get(i) instanceof Penguin){
				countPenguin++;
				countAnimals++;
			}
			if (animals.get(i) instanceof Taipan){
				countTaipan++;
				countAnimals++;
			}
			if (animals.get(i) instanceof Cobra){
				countCobra++;
				countAnimals++;
			}
			if (animals.get(i) instanceof Lion){
				countLion++;
				countAnimals++;
			}
			if (animals.get(i) instanceof Tiger){
				countTiger++;
				countAnimals++;
			}
			if (animals.get(i) instanceof Bear) {
				countBear++;
				countAnimals++;
			}
		}

		// print the summary of species
		System.out.println("\nSummary List:");
		System.out.println("There are " + countAnimals + " animals in the zoo.");
		System.out.println(countEagle + " eagle(s)");
		System.out.println(countPenguin + " penguin(s)");
		System.out.println(countTaipan + " taipan(s)");
		System.out.println(countCobra + " cobra(s)");
		System.out.println(countLion + " lion(s)");
		System.out.println(countTiger + " tiger(s)");
		System.out.println(countBear + " bear(s)");


	} // end of printSummaryView


	public static void printVerboseList(ArrayList<Animal> animals){

		System.out.println("\nVerbose List:");
		for(int i=0; i < animals.size(); i++){
			int num = i + 1;
			System.out.println(num + ". " + animals.get(i).verbose());
		}

	} // end of printVerboseList

	public static ArrayList<Animal> checkAnimalInput(ArrayList<Animal> alist, String option){
		// check the user's input
		// if add, the create an animal and call addAnimal
		Scanner sc = new Scanner(System.in);
		option = option.toLowerCase();
		if (option.equals("add")) {
			System.out.println("Enter the type of animal followed by the name of the animal: ");
			String type = sc.next();
			String name = sc.next();
			alist = addAnimal(type, name, alist);
		}
		else if (option.equals("delete")){
			System.out.println("Enter the type and name of the animal you would like to delete");
			String type = sc.next();
			String name = sc.next();
			alist = deleteAnimal(type, name, alist);
		}
		else if (option.equals("display")) {
			System.out.println("Enter the type and name of the animal you would like to display");
			String type = sc.next();
			String name = sc.next();
			displayAnimal(type, name, alist);
		}
		else if (option.equals("danger")) {
			System.out.println("Enter the type and name of the animal you would like to check");
			String type = sc.next();
			String name = sc.next();
			checkDanger(type, name, alist);
		}
		else {
			System.out.println(option + " is not a correct option. Please try again");
			option = sc.nextLine();
		}

		return alist;
	} // end of checkAnimalInput

	public static ArrayList<Animal> addAnimal(String type, String animalName, ArrayList<Animal> alist){
		// adding a to alist, possibly resizing
		Animal a = new Animal(); //reference type animal
		// create species based object using new and checking user input
		// this will be an if/elseif for every species type
		type = type.toLowerCase();

		if (type.equals("eagle")){
			a = new Eagle(animalName, type); // here we bind 'a' to Object type Dog
			// do not say Dog d = new Dog();
		}
		else if (type.equals("penguin")){
			a = new Penguin(animalName, type);
		}
		else if (type.equals("taipan")){
			a = new Taipan(animalName, type);
		}
		else if (type.equals("cobra")){
			a = new Cobra(animalName, type);
		}
		else if (type.equals("lion")){
			a = new Lion(animalName, type);
		}
		else if (type.equals("tiger")){
			a = new Tiger(animalName, type);
		}
		else if (type.equals("bear")) {
			a = new Bear(animalName, type);
		}
		alist.add(a);
		return alist; // updated alist
	}

	public static ArrayList<Animal> deleteAnimal(String type, String name, ArrayList<Animal> alist) {
		for(int i=0; i < alist.size(); i++){

			if ((name.toLowerCase().equals(alist.get(i).getName().toLowerCase())) && (type.toLowerCase().equals(alist.get(i).getType().toLowerCase()))) {
				System.out.println("Deleting " + name + " the " + type);
				alist.remove(i);
			}
		}
		return alist;
	}
	public static void displayAnimal(String type, String name, ArrayList<Animal> alist) {
		for(int i=0; i < alist.size(); i++){

			if ((name.toLowerCase().equals(alist.get(i).getName().toLowerCase())) && (type.toLowerCase().equals(alist.get(i).getType().toLowerCase()))) {
				System.out.println(alist.get(i).moreInfo());
			}
		}
	}
	public static void checkDanger(String type, String name, ArrayList<Animal> alist) {
		for(int i=0; i < alist.size(); i++){

			if ((name.toLowerCase().equals(alist.get(i).getName().toLowerCase())) && (type.toLowerCase().equals(alist.get(i).getType().toLowerCase()))) {
				if(!alist.get(i).isDangerous()) {
					System.out.println("Animal is Dangerous!");
				}
				else {
					System.out.println("Animal isn't Dangerous!");
				}
			}
		}
	}

	public static void main(String[] args){
		// setup initial animals
		Scanner sc = new Scanner(System.in);

		ArrayList<Animal> alist = new ArrayList<Animal>();

		try {
			alist = setupAnimals();
		}
		catch(Exception ex) {
			System.out.println("Exception Caught");
		}

		// print summary view
		printSummaryView(alist);

		// print Verbose list
		printVerboseList(alist);

		// interactive add/delete/display
		String input = "";
		while(!input.toLowerCase().equals("exit")){
			System.out.println("What would you like to do (add/danger/delete/display/exit)? ");
			input = sc.nextLine(); // next(); //hasNext();
			if (input.equals("exit")) {
				System.out.println("Thanks for visiting our Zoo!");
				break;
			}
			else {
				alist = checkAnimalInput(alist, input);
				printSummaryView(alist);
			}
		}
	}
}
