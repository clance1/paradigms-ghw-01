package zoo.animals;
import zoo.animals.Snake;

public class Cobra extends Snake {
  boolean has_hood = true;
  String type = "Cobra";
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  	+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
		+ this.continent + "\nVertebrate " + this.vertebrate + "\nHas Scales: "
		+ this.has_scales + "\nLays Eggs: " + this.lays_eggs + "\nVenom: " + this.venom
		+ "\nTriangularHead: " + this.triangularHead + "\nHas Hood: " + has_hood;

  public Cobra(String name, int age, String conservation_status, boolean babies, String continent, boolean vertebrate, boolean has_scales, boolean lays_eggs, boolean venom, boolean triangularHead, boolean has_hood){
    super(name, age, conservation_status, babies, continent, vertebrate, has_scales, lays_eggs, venom, triangularHead);
    this.has_hood = true;
    this.type = "cobra";
  }
  
  public Cobra(String name, String type) {
    super(name, type);
  }

  public boolean getHas_Hood() {
    return has_hood;
  }

  public void setHas_Hood(boolean has_hood)  {
    this.has_hood = has_hood;
  }

  public String moreInfo() {
    return moreInfo;
  }
}
