package zoo.animals;
import zoo.animals.Animal;

public class Reptile extends Animal {
  boolean vertebrate = true;
  boolean has_scales = true;
  boolean lays_eggs = true;
  String type;
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  	+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
		+ this.continent + "\nVertebrate " + this.vertebrate + "\nHas Scales: "
		+ this.has_scales + "\nLays Eggs: " + this.lays_eggs;

  public Reptile(String name, int age, String conservation_status, boolean babies, String continent, boolean vertebrate, boolean has_scales, boolean lays_eggs){
    super(name, age, conservation_status, babies, continent);
    this.vertebrate = true;
    this.has_scales = true;
    this.lays_eggs = true;
    this.type = "reptile";
  }

  public Reptile(String name, String type) {
    super(name, type);
  }

  public boolean getVertebrate() {
    return vertebrate;
  }

  public void setVertebrate(boolean vertebrate)  {
    this.vertebrate = vertebrate;
  }

  public boolean getHas_Scales() {
    return has_scales;
  }

  public void setHas_Scales(boolean has_scales)  {
    this.has_scales = has_scales;
  }

  public boolean getLays_Eggs() {
    return lays_eggs;
  }

  public void setLays_Eggs(boolean lays_eggs)  {
    this.lays_eggs = lays_eggs;
  }
}
