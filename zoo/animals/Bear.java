package zoo.animals;
import zoo.animals.Mammal;

public class Bear extends Mammal {
  boolean hibernates = true;
  String type = "Bear";
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: " + this.conservation_status + "\nBabies: " + this.babies + "\nLocation: " + this.continent + "\nWarm Blooded: " + this.warm_blooded + "\nLive Birth: " + this.live_birth + "\nHas Hair: " + this.has_hair + "\nHas Claws: " + this.has_claws + "Has Sharp Teeth: " + this.has_sharp_teeth + "\nHibernates: " + this.hibernates;

  public Bear(String name, int age, String conservation_status, boolean babies, String continent, boolean warm_blooded, boolean live_birth, boolean has_hair, boolean has_claws, boolean has_sharp_teeth, boolean hibernates){
    super(name, age, conservation_status, babies, continent, warm_blooded, live_birth, has_hair, has_claws, has_sharp_teeth);
    this.hibernates = true;
    this.type = "bear";
  }


  public Bear(String name, String type) {
    super(name, type);
  }

  public boolean getHibernates() {
    return hibernates;
  }

  public void setHibernates(boolean hibernates)  {
    this.hibernates = hibernates;
  }

  public boolean isDangerous() {
    if (has_sharp_teeth && has_claws && !hibernates) {
      return true;
    }
    return false;
  }
}
