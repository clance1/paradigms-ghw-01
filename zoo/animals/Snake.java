package zoo.animals;
import zoo.animals.Reptile;

public class Snake extends Reptile {
  boolean venom = true;
  boolean triangularHead = true;
  String type;
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  	+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
		+ this.continent + "\nVertebrate " + this.vertebrate + "\nHas Scales: "
		+ this.has_scales + "\nLays Eggs: " + this.lays_eggs + "\nVenom: " + this.venom
		+ "\nTriangularHead: " + this.triangularHead;

  public Snake(String name, int age, String conservation_status, boolean babies, String continent, boolean vertebrate, boolean has_scales, boolean lays_eggs, boolean venom, boolean triangularHead){
    super(name, age, conservation_status, babies, continent, vertebrate, has_scales, lays_eggs);
    this.venom = true;
    this.triangularHead = true;
    this.type = "snake";
  }
  
  public Snake(String name, String type) {
    super(name, type);
  }

  public boolean getVenom() {
    return venom;
  }

  public void setVenom(boolean venom)  {
    this.venom = venom;
  }

  public boolean getTriangularHead() {
    return triangularHead;
  }

  public void setTriangular(boolean triangularHead)  {
    this.triangularHead = triangularHead;
  }

  public boolean isDangerous() {
    return true;
  }
}
