package zoo.animals;
import zoo.animals.Tiger;

public class Tiger extends Cat {
  boolean striped = true;
  String type = "Tiger";
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  		+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
			+ this.continent + "\nWarm Blooded: " + this.warm_blooded + "\nLive Birth: "
			+ this.live_birth + "\nHas Hair: " + this.has_hair + "\nHas Claws: "
			+ this.has_claws + "\nHas Sharp Teeth: "
			+ this.has_sharp_teeth +  "\nHas Whiskers: "
			+ this.has_whiskers + "\nHas Stripes: " + this.striped;

  public Tiger(String name, int age, String conservation_status, boolean babies, String continent, boolean warm_blooded, boolean live_birth, boolean has_hair, boolean has_claws, boolean has_sharp_teeth, boolean has_whiskers, boolean domesticated, boolean striped){
    super(name, age, conservation_status, babies, continent, warm_blooded, live_birth, has_hair, has_claws, has_sharp_teeth, has_whiskers, domesticated);
    this.striped = striped;
    this.type = "tiger";
  }

  public Tiger(String name, String type) {
    super(name, type);
  }

  public boolean getStriped() {
    return striped;
  }

  public void setStriped(boolean striped)  {
    this.striped = striped;
  }

  public String moreInfo() {
    return moreInfo;
  }

  public boolean isDangerous() {
    if (!domesticated) {
      return false;
    }
    return true;
  }
}
