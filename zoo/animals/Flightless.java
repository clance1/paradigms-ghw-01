package zoo.animals;
import zoo.animals.Bird;

public class Flightless extends Bird {
  boolean vegetarian = true;
  String type;
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  	+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
		+ this.continent + "\nLays Eggs: " + this.lays_eggs + "\nHas Feathers: "
		+ this.has_feathers + "\nHas Wings: " + this.has_wings + "\nVegetarian : "
		+ this.vegetarian;

  public Flightless(String name, int age, String conservation_status, boolean babies, String continent, boolean lays_eggs, boolean has_feathers, boolean has_wings, boolean flightless){
    super(name, age, conservation_status, babies, continent, lays_eggs, has_feathers, has_wings);
    this.vegetarian = true;
    this.type = "flightless bird";
  }

  public Flightless(String name, String type) {
    super(name, type);
  }

  public boolean getVegetarian() {
    return vegetarian;
  }

  public void setVegetarian(boolean vegetarian)  {
    this.vegetarian = vegetarian;
  }
}
