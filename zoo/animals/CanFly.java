package zoo.animals;
import zoo.animals.Bird;

public class CanFly extends Bird {
  boolean flight = true;
  String type;

  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  	+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
		+ this.continent + "\nLays Eggs: " + this.lays_eggs + "\nHas Feathers: "
		+ this.has_feathers + "\nHas Wings: " + this.has_wings + "\nCan Fly: "
		+ this.flight;

  public CanFly(String name, int age, String conservation_status, boolean babies, String continent, boolean lays_eggs, boolean has_feathers, boolean has_wings, boolean flight){
    super(name, age, conservation_status, babies, continent, lays_eggs, has_feathers, has_wings);
    this.flight = true;
    this.type = "canfly";
  }

  public CanFly(String name, String type) {
    super(name, type);
  }

  public boolean getFlight() {
    return flight;
  }

  public void setFlight(boolean flight)  {
    this.flight = flight;
  }
}
