package zoo.animals;
import zoo.animals.Flightless;

public class Penguin extends Flightless {
  boolean canSwim = true;
  String type = "Penguin";
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  	+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
		+ this.continent + "\nLays Eggs: " + this.lays_eggs + "\nHas Feathers: "
		+ this.has_feathers + "\nHas Wings: " + this.has_wings + "\nVegetarian : "
		+ this.vegetarian + "\nCan Swim: " + this.canSwim;

  public Penguin(String name, int age, String conservation_status, boolean babies, String continent, boolean lays_eggs, boolean has_feathers, boolean has_wings, boolean flightless, boolean vegetarian, boolean canSwim){
    super(name, age, conservation_status, babies, continent, lays_eggs, has_feathers, has_wings, vegetarian);
    this.canSwim = true;
    this.type = "penguin";
  }

  public Penguin(String name, String type) {
    super(name, type);
  }

  public boolean getCanSwim() {
    return canSwim;
  }

  public void setCanSwim(boolean canSwim)  {
    this.canSwim = canSwim;
  }
  public String moreInfo() {
    return moreInfo;
  }
  public boolean isDangerous() {
    if (vegetarian && canSwim) {
      return false;
    }
    return true;
  }
}
