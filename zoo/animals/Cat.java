package zoo.animals;
import zoo.animals.Mammal;

public class Cat extends Mammal {
  boolean has_whiskers = true;
  boolean domesticated = false;
  String type;
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: " + this.conservation_status + "\nBabies: " + this.babies + "\nLocation: " + this.continent + "\nWarm Blooded: " + this.warm_blooded + "\nLive Birth: " + this.live_birth + "\nHas Hair: " + this.has_hair + "\nHas Claws: " + this.has_claws + "\nHas Sharp Teeth: " + this.has_sharp_teeth + "\nHas Whiskers: " + this.has_whiskers;


  public Cat(String name, int age, String conservation_status, boolean babies, String continent, boolean warm_blooded, boolean live_birth, boolean has_hair, boolean has_claws, boolean has_sharp_teeth, boolean has_whiskers, boolean domesticated){
    super(name, age, conservation_status, babies, continent, warm_blooded, live_birth, has_hair, has_claws, has_sharp_teeth);
    this.has_whiskers = has_whiskers;
    this.domesticated = domesticated;
    this.type = "cat";
  }

  public Cat(String name, String type) {
    super(name, type);
  }

  public boolean getHas_Whiskers() {
    return has_whiskers;
  }

  public void setHas_Whiskers(boolean has_whiskers)  {
    this.has_whiskers = has_whiskers;
  }

  public boolean getDomesticated() {
    return domesticated;
  }

  public void setDomesticated(boolean domesticated)  {
    this.domesticated = domesticated;
  }
}
