package zoo.animals;
import zoo.animals.Snake;

public class Taipan extends Snake {
  boolean changesColor = true;
  String type = "Taipan";
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  	+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
		+ this.continent + "\nVertebrate " + this.vertebrate + "\nHas Scales: "
		+ this.has_scales + "\nLays Eggs: " + this.lays_eggs + "\nVenom: " + this.venom
		+ "\nTriangularHead: " + this.triangularHead + "\nChanges Color: " + this.changesColor;

  public Taipan(String name, int age, String conservation_status, boolean babies, String continent, boolean vertebrate, boolean has_scales, boolean lays_eggs, boolean venom, boolean triangularHead, boolean changesColor){
    super(name, age, conservation_status, babies, continent, vertebrate, has_scales, lays_eggs, venom, triangularHead);
    this.changesColor = true;
    this.type = "taipan";
  }

  public Taipan(String name, String type) {
    super(name, type);
  }

  public boolean getChangesColor() {
    return changesColor;
  }

  public void setChangesColor(boolean changesColor)  {
    this.changesColor = changesColor;
  }
  public String moreInfo() {
    return moreInfo;
  }
}
