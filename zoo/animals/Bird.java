package zoo.animals;
import zoo.animals.Animal;

public class Bird extends Animal {
  boolean lays_eggs = true;
  boolean has_feathers = true;
  boolean has_wings = true;
  String type;

  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  	+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
		+ this.continent + "\nLays Eggs: " + this.lays_eggs + "\nHas Feathers: "
		+ this.has_feathers + "\nHas Wings: " + this.has_wings;

  public Bird(String name, int age, String conservation_status, boolean babies, String continent, boolean lays_eggs, boolean has_feathers, boolean has_wings){
    super(name, age, conservation_status, babies, continent);
    this.lays_eggs = true;
    this.has_feathers = true;
    this.has_wings = true;
    this.type = "bird";
  }

  public Bird(String name, String type) {
    super(name, type);
  }

  public boolean getLays_Eggs() {
    return lays_eggs;
  }

  public void setLays_Eggs(boolean lays_eggs)  {
    this.lays_eggs = lays_eggs;
  }

  public boolean getHas_Feathers()  {
    return has_feathers;
  }

  public void setHas_Feathers(boolean has_feathers)  {
    this.has_feathers = has_feathers;
  }

  public boolean getHas_Wings()  {
    return has_feathers;
  }

  public void setHas_Wings(boolean has_wings)  {
    this.has_wings = has_wings;
  }
}
