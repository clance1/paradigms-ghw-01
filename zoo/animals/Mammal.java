package zoo.animals;
import zoo.animals.Animal;

public class Mammal extends Animal {
  boolean warm_blooded = true;
  boolean live_birth = true;
  boolean has_hair = true;
  boolean has_claws = true;
  boolean has_sharp_teeth = true;
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: " + this.conservation_status + "\nBabies: " + this.babies + "\nLocation: " + this.continent + "\nWarm Blooded: " + this.warm_blooded + "\nLive Birth: " + this.live_birth + "\nHas Hair: " + this.has_hair + "\nHas Claws: " + this.has_claws + "Has Sharp Teeth: " + this.has_sharp_teeth;


  public Mammal(String name, int age, String conservation_status, boolean babies, String continent, boolean warm_blooded, boolean live_birth, boolean has_hair, boolean has_claws, boolean has_sharp_teeth){
    super(name, age, conservation_status, babies, continent);
    this.warm_blooded = warm_blooded;
    this.live_birth = live_birth;
    this.has_hair = has_hair;
    this.has_claws = has_claws;
    this.has_sharp_teeth = has_sharp_teeth;
    this.type = "mammal";
  }

  public Mammal(String name, String type) {
    super(name, type);
  }

  public boolean getWarm_Blooded() {
    return warm_blooded;
  }

  public void setWarm_Blooded(boolean warm_Blooded)  {
    this.warm_blooded = warm_blooded;
  }

  public boolean getLive_Birth() {
    return live_birth;
  }

  public void setLive_Birth(boolean live_birth)  {
    this.live_birth = live_birth;
  }

  public boolean has_hair() {
    return has_hair;
  }

  public void setHas_Hair(boolean has_hair)  {
    this.has_hair = has_hair;
  }

  public boolean getHas_Claws() {
    return has_claws;
  }

  public void setHas_Claws(boolean has_claws)  {
    this.has_claws = has_claws;
  }

  public boolean getHas_Sharp_Teeth() {
    return has_sharp_teeth;
  }

  public void setLays_Eggs(boolean has_sharp_teeth)  {
    this.has_sharp_teeth = has_sharp_teeth;
  }
}
