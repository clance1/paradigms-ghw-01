package zoo.animals;

public class Animal {
  String name;
  int age = 0;
  String conservation_status = "N/A";
  boolean babies = false;
  String continent = "N/A";
  String gender = "N/A";
  String type;
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: " + this.conservation_status + "\nBabies: " + this.babies + "\nLocation: " + this.continent;

  public Animal(){}

  public Animal(String name, int age, String conservation_status, boolean babies, String continent) {
    this.name = name;
    this.age = age;
    this.conservation_status = conservation_status;
    this.babies = babies;
    this.continent = continent;
    this.type = "animal";
  }

  public Animal(String name, String type) {
    this.name = name;
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public String getType() {
    return type;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age)  {
    this.age = age;
  }

  public String getConservation_status() {
    return conservation_status;
  }

  public void setConservation_status(String conservation_status) {
    this.conservation_status = conservation_status;
  }

  public boolean getBabies() {
    return babies;
  }

  public void setBabies(boolean babies)  {
    this.babies = babies;
  }

  public String getContinent() {
    return continent;
  }

  public void setContinent(String continent)  {
    this.continent = continent;
  }

  public String verbose() {
    String verbose = "Name: " + this.name + "\nSpecies: " + this.type + "\n";
    return verbose;
  }

  public String moreInfo() {
    return moreInfo;
  }

  public boolean isDangerous() {
    return false;
  }
}
