package zoo.animals;
import zoo.animals.CanFly;

public class Eagle extends CanFly {
  boolean sharpBeak = true;
  String type = "Eagle";
  String moreInfo = "Name: " + this.name + "\nType: " + this.type + "\nConservation Status: "
	  	+ this.conservation_status + "\nBabies: " + this.babies + "\nLocation: "
		+ this.continent + "\nLays Eggs: " + this.lays_eggs + "\nHas Feathers: "
		+ this.has_feathers + "\nHas Wings: " + this.has_wings + "\nCan Fly: "
		+ this.flight + "\nSharp Beak: " + this.sharpBeak;

  public Eagle(String name, int age, String conservation_status, boolean babies, String continent, boolean lays_eggs, boolean has_feathers, boolean has_wings, boolean flight, boolean sharpBeak){
    super(name, age, conservation_status, babies, continent, lays_eggs, has_feathers, has_wings, flight);
    this.sharpBeak = true;
    this.type = "eagle";
  }

  public Eagle(String name, String type) {
    super(name, type);
  }

  public boolean getSharpBeak() {
    return sharpBeak;
  }

  public void setSharpBeak(boolean sharpBeak)  {
    this.sharpBeak = sharpBeak;
  }

  public String moreInfo() {
    return moreInfo;
  }

  public boolean isDangerous() {
    if (sharpBeak && flight) {
      return true;
    }
    return false;
  }
}
