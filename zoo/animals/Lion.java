package zoo.animals;
import zoo.animals.Cat;

public class Lion extends Cat {
  boolean mane = true;
  String type = "Lion";
  String moreInfo = "Name: " + this.name + "\nType: " + this.type
	  	+ "\nConservation Status: " + this.conservation_status + "\nBabies: "
		+ this.babies + "\nLocation: "
		+ this.continent + "\nWarm Blooded: "
		+ this.warm_blooded + "\nLive Birth: "
		+ this.live_birth + "\nHas Hair: "
		+ this.has_hair + "\nHas Claws: "
		+ this.has_claws + "\nHas Sharp Teeth: "
		+ this.has_sharp_teeth +  "\nHas Whiskers: "
		+ this.has_whiskers + "\nHas Mane: " + this.mane;

  public Lion(String name, int age, String conservation_status, boolean babies, String continent, boolean warm_blooded, boolean live_birth, boolean has_hair, boolean has_claws, boolean has_sharp_teeth, boolean has_whiskers, boolean domesticated, boolean mane){
    super(name, age, conservation_status, babies, continent, warm_blooded, live_birth, has_hair, has_claws, has_sharp_teeth, has_whiskers, domesticated);
    this.mane = true;
    this.type = "lion";
  }

  public Lion(String name, String type) {
    super(name, type);
  }

  public boolean getMane() {
    return mane;
  }

  public void setMane(boolean mane)  {
    this.mane = mane;
  }

  public String moreInfo() {
    return moreInfo;
  }

  public boolean isDangerous() {
    if (!domesticated) {
      return false;
    }
    return true;
  }
}
